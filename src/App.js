import "./App.css";
//import "./style/index.scss";
import "./App.scss";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import setAuthToken from "./utils/setAuthToken";
import {
  setCurrentUser,
  logoutUser,
} from "./actions/authActions";
import store from "./store";
import CONSTANTS from "./common/constants.json";
import Login from "./Components/Login"
import Register from "./Components/Register";
import Sidebar from "./Components/Sidebar"
import Navbar from "./Components/Navbar"
// import Footer from "./Components/Footer"
// import Dashboard from "./Components/Dashboard"
// Check for token
if (localStorage.access_token) {
  // Set auth token header auth
  setAuthToken(localStorage.access_token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.access_token);
  // Set user and isAuthenticated
  //console.log("decoded", decoded)
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Clear current Profile
    // store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = CONSTANTS.ROUTES.LOGIN;
  }
}

class App extends React.Component {
  render() {
    const {LOGIN, REGISTER} = CONSTANTS.ROUTES
    return (
      <Provider store={store}>
      <Router>
        <div className="">
          <ToastContainer/>
          <div className="dashboard--container">
            {/* <Dashboard /> */}
            <Navbar />
            <div className="dashboard--flex__container">
            <Sidebar />
            </div>
            <div className="page-content">  
              <Switch>
                <Route
                  path={LOGIN}
                  component={Login}
                />
                <Route
                
                  path={REGISTER}
                  component={Register}
                />
              </Switch>
              <Switch>
                <Route
                  exact
                  path={CONSTANTS.ROUTES.REGISTER}
                  component={Register}
                />
              </Switch>
              <Switch>
                {/* <PrivateRoute
                  exact
                  path={CONSTANTS.ROUTES.DASHBOARD}
                  component={() => (
                    <Dashboard profile={this.props.currentUserProfile} />
                  )}
                /> */}
              </Switch>
            </div>
            {/* <Footer /> */}
          </div>
        </div>
      </Router>
      <Helmet>
          <script src="/assets/js/main.js"></script>
        </Helmet>
    </Provider>
  
  
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.auth
  };
};


export default connect(mapStateToProps)(App);

