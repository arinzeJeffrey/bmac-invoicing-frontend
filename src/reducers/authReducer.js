import isEmpty from "../utils/isEmpty";
import { LOG_OUT_USER, SET_CURRENT_USER } from "../actions/types";

const initialState = {
    isAuthenticated: false,
    user: {},
};

export default function auth (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload,
            };
        case LOG_OUT_USER:
            return {
                ...state,
                user: {},
            };
        default:
            return state;
    }
}