
import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import isLoadingReducer from "./isLoadingReducer"
import authReducer from "./authReducer"
import networkResponse from "./networkResponse"


export default combineReducers({
    networkResponse,
    auth:authReducer,
    errors: errorReducer,
    isLoading: isLoadingReducer,

});