export const profile = {
    first_name: 'Mikky',
    last_name: 'Mouse',
    email: 'mike@gmail.com'
}

export const company = {
    company_name: "Mikky And Co",
    rc_number: '32573',
    email: "mike@gmail.com"
}