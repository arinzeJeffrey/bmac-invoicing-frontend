import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import process from "../env";
import { SET_CURRENT_USER, IS_LOADING, NOT_LOADING, LOG_OUT_USER, REDIRECT_TO, SHOW_NETWORK_RESPONSE } from "./types";
import { notify } from "../utils/notification";
import CONSTANTS from "../common/constants.json"
import { clearNetworkResponse } from "./networkActions";

const { DASHBOARD } = CONSTANTS.ROUTES

const service_url = process.env.SERVICE_URL;


export const test = (data) => {
    return async(dispatch) => {
        try {
            dispatch({ type: IS_LOADING })
            const signUp = await axios.post(`${service_url}`, data)
            dispatch({ type: NOT_LOADING })
            dispatch({ type: REDIRECT_TO, payload: DASHBOARD})
            dispatch(clearNetworkResponse())
            return notify(signUp.res.data.message, "success")
        } catch (error) {
            if(error.response){
                dispatch({ type: SHOW_NETWORK_RESPONSE, payload: error.response.data[0]})
                dispatch(clearNetworkResponse())
            }else{
                return notify("Check your internet", "error")
            }
        }
    }
}



// Login - Get User Token
export const loginUser = (data) => {
    return async(dispatch) => {
        try {
            dispatch({ type: IS_LOADING })
            const login = await axios.post(`${service_url}login`, data)
            dispatch({ type: NOT_LOADING })
            // Save to localStorage
            const { access_token } = login.data;
            localStorage.setItem("access_token", access_token);
            // Set token to Auth header
            setAuthToken(access_token);
            // Decode token to get user data
            const decoded = jwt_decode(access_token);
            // Set current user
            dispatch(setCurrentUser(decoded));
            dispatch({ type: REDIRECT_TO, payload: DASHBOARD})
            dispatch(clearNetworkResponse())
            return notify("Logged In", "success")
        } catch (error) {
            if(error.response){
                dispatch({ type: SHOW_NETWORK_RESPONSE, payload: error.response.data[0]})
                dispatch(clearNetworkResponse())
            }else{
                return notify("Check your internet", "error")
            }
        }
    }
};

// Set logged in user
export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded,
    };
};




// Log user out
export const logoutUser = () => (dispatch) => {
    dispatch({
            type: LOG_OUT_USER
        })
        // Remove token from localStorage
    localStorage.removeItem("jwtToken");
    localStorage.clear()
        // Remove auth header for future requests
    setAuthToken(false);
    // Set current user to {} which will set isAuthenticated to false
    dispatch(setCurrentUser({}));
    // window.location.href = LOGIN
};


