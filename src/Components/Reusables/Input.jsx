import React from 'react'

export default function Input({getInputs, label, type="text", placeholder="", title, feedbackText="", inputErr, ...rest}) {

    return (
    <div className="my-2 inputs">
        <label htmlFor={title}>{title}</label>
            <input type={type} 
                onChange={getInputs}
                className={`form-control ${ inputErr ? 'is-invalid':''}`} 
                placeholder ={placeholder}
                id={title} 
                {...rest}
                required 
            />
        <div className={ `d-inline ${ inputErr ? "invalid-feedback":""}`}>
            { feedbackText}
        </div>
    </div>
    )
}
