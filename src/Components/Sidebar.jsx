import React, { Component } from "react";
import navitems from "./Reusables/NonComponent/navitems";
import NavItem from "./Reusables/Navitem";
//import { toggleSVG } from "./Reusables/NonComponent/svgs";
import { connect } from "react-redux";
// import { SidebarLazyLoads } from "./Reusables/LazyLoads";
class Sidebar extends Component {
  render() {
    const _NavItem = navitems.map((item, index) => (
      <NavItem key={index} 
      svg={item.svg} 
      name={item.name} 
      path={item.path} 
      />
    ));
    // const _LazySidebar = <SidebarLazyLoads />;
    const { isAuthenticated } = this.props?.auth ?? false;
    //const  isAuthenticated  = true;
  console.log("my props", this.props)
    let renderable;
    if (isAuthenticated) {
      renderable = (
        <nav className="side-nav"> 
          <div className="inner-nav mt-5">        
            {_NavItem}    
          </div>
                </nav>
      );
    } else {
      renderable = (
        <nav className="sidebar">
        </nav>
      );
    }

    return <>{renderable}</>;
  }
}

const mapStateToProps = ({ auth, currentUserProfile }) => ({
  auth,
  currentUserProfile
});
export default connect(mapStateToProps)(Sidebar);
