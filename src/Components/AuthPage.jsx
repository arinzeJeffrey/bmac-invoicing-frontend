import React, { Component } from 'react';
import Login from "./Login"
import Signup from "./Signup"

export default class AuthPage extends Component {
    render() {
        return (
            <div className="auth--container">
            <div className="logo">
              <img src="../assets/images/bmac-logo 1.png" alt="" srcset="" />
            </div>
    
            <div className="header-text">
              <h1 className="h1-light">Try out BMAC invoicing - it's free</h1>
    
              <p className="text">
                BMAC Invoicing helps Small businesses, consultants, and Entrepreneur
                around the world simplify their finances Processes
              </p>
            </div>

            </div>
        )
    }
}
