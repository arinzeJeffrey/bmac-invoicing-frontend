import React, { useState } from 'react'
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Logo from "../assets/images/bmac.png"
import { loginUser }from "../actions/authActions"
import Input from "../Components/Reusables/Input"
//import isEmpty from "../utils/isEmpty";


const Login = (props) => {
  const [userInput, setUserInput] = useState({ email: "", password: ""})
  const { dispatch, networkResponse } = props
  const handleChange = (e) => {
    setUserInput({...userInput, [e.target.name]: e.target.value })
  }
  const handleSubmit = e => {
    e.preventDefault()
    dispatch(loginUser(userInput))
  }
  const { email, password } = userInput
  if(networkResponse.redirectTo){
    return <Redirect to={networkResponse.redirectTo}/>
  }
  return (
    <div className=" overflow-hidden"> 
    <ToastContainer/>
      <div className="auth--container">
        <div className="logo">
        <img
          src={Logo}
          alt="bmac logo"
          srcSet=""
          width="60%"
        />
        </div>
        <div className="form--container">
          <div className="login-header">
            <h1 className="h1-bold">Sign In</h1>
            <h3 className="h3-medium">
              Not a member?<span className="green-text">Sign up</span>
            </h3>
          </div>
        <form onSubmit={handleSubmit} >
          <Input
            type="email"
            name={"email"}
            value={email}
            getInputs={handleChange}
            inputErr={networkResponse.response ? true:false}
            title={"Email"} 
            feedbackText=""
          />
          <Input
            type="password"
            name="password"
            value={password}
            getInputs={handleChange}
            inputErr={networkResponse.response ? true:false}
            title={"Password"} 
            feedbackText={ networkResponse.response ? networkResponse.response: ""}
          /> 
          <button 
            className="btn btn-success my-3 text-capitalize d-flex mx-auto justify-content-center px4half py-2"
            style={{borderRadius:"2rem"}}
          > Sign in
          </button>
          </form>
        
        </div>
      </div>
    </div>
    )
}



const mapStateToProps = state => {
  const {isLoading, errors, networkResponse} = state
  return {
    isLoading,
    errors,
    networkResponse,
  };
};

export default connect(mapStateToProps)(Login);