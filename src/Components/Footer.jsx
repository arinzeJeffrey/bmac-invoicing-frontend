import React from 'react'
import {NavLink} from "react-router-dom"

export default function footer() {
   
    return (
        <footer className="footer__container" style={{
            position:"sticky",
            bottom:"0"
           
        }}>
        <p className="text">
        © 
          <NavLink className="green-text" to="https://ikooba.com/" target="_blank">
            Ikooba Technologies
          </NavLink>
          {" "}{new Date().getFullYear()}
        </p>
      </footer>
    )
}
