import React, { useState } from 'react'
import { useDispatch } from "react-redux"

const Signup = () => {
  const [userInput, setUserInput] = useState({ email: "", password: ""})
  const dispatch = useDispatch()
  const handleChange = (e) => {
    setUserInput({ [e.target.name]: e.target.value })
    console.log(userInput);
  }
  const handleSubmit = e => {
    e.preventDefault()
    dispatch(test(userInput))
  }
  return (
  <form onSubmit={handleSubmit} className="auth--container">
    <div className="form--container">
      <div className="login-header">
        <h1 className="h1-bold">Sign In</h1>
        <h3 className="h3-medium">
          Not a member?<span className="green-text">Sign up</span>
        </h3>
      </div>
      <label htmlFor=""> Email </label>
      <input onChange={handleChange} type="text" name="email" className="input email" />
      <label htmlFor=""> Password </label>
      <input onChange={handleChange} type="text" name="password" className="input password" />
      <p className="error text">
        Field must contain at least one capital letter, lower case letter
        and number
      </p>
      <label htmlFor=""> Confirm Password </label>
      <input type="text" className="input password-confirm" />
      <a href="#" className="btn-green">Sign in</a>
    </div>
  </form>
  )
}


export default Signup
